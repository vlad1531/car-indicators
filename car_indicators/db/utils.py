from types import SimpleNamespace
from typing import Optional, Union
from pathlib import Path

from alembic.config import Config

PROJECT_PATH = Path(__file__).parent.parent.resolve()

def make_alembic_config(cmd_opts: SimpleNamespace, base_path: Union[Path, str] = PROJECT_PATH) -> Config:

    cmd_opts.config = Path(cmd_opts.config)
    base_path = Path(base_path)

    if not cmd_opts.config.is_absolute():
        cmd_opts.config = base_path/cmd_opts.config
    
    config = Config(file_=cmd_opts.config, ini_section=cmd_opts.name, cmd_opts=cmd_opts)

    alembic_location = Path(config.get_main_option('script_location'))
    if not alembic_location.is_absolute():
        config.set_main_option('script_location', str(base_path/alembic_location))

    if cmd_opts.pg_url:
        config.set_main_option('sqlalchemy.url', str(cmd_opts.pg_url))
    
    return config

def alembic_config_from_url(pg_url: Optional[str] = None):
    cmd_options = SimpleNamespace(
        config='alembic.ini', name='alembic', pg_url=pg_url,
        raiseerr=False, x=None,
    )
    return make_alembic_config(cmd_options)