import pytest
from alembic.command import downgrade, upgrade
from alembic.config import Config
from alembic.script import Script, ScriptDirectory


from car_indicators.db.utils import alembic_config_from_url

@pytest.fixture
def revisions():
   
    config = alembic_config_from_url()

    revisions_dir = ScriptDirectory.from_config(config)
    
    revisions = list(revisions_dir.walk_revisions('base', 'heads'))
    revisions.reverse()
    return revisions


def test_migrations_stairway(alembic_config: Config, revisions: Script):
    upgrade(alembic_config, revisions[0].revision)

    downgrade(alembic_config, revisions[0].down_revision or '-1')
    upgrade(alembic_config, revisions[0].revision)
