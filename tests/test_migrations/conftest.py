import os
import pytest
from contextlib import contextmanager
import uuid
from sqlalchemy_utils import create_database, drop_database
from sqlalchemy.ext.asyncio import create_async_engine

from car_indicators.db.utils import alembic_config_from_url

DEFAULT_URL = 'postgresql+asyncpg://some_user:some_password@localhost:5432/database'

@contextmanager
def tmp_database(db_url: str, suffix: str = '', **kwargs):
    tmp_db_name = '.'.join([uuid.uuid4().hex, 'car_indicators', suffix])
    tmp_db_url = f'{db_url}/{tmp_db_name}'
    create_database(tmp_db_url, **kwargs)

    try:
        yield tmp_db_url
    finally:
        drop_database(tmp_db_url)

@pytest.fixture
def pg_url():
    return os.getenv('CI_BD_URL', DEFAULT_URL)

@pytest.fixture
def postgres(pg_url):
    """
    Creates empty temporary database.
    """
    with tmp_database(pg_url, 'pytest') as tmp_url:
        yield tmp_url

@pytest.fixture
def postgres_engine(postgres):
    engine = create_async_engine(postgres, future=True, echo=True)
    try:
        yield engine
    finally:
        engine.dispose()

@pytest.fixture
def alembic_config(postgres):
    return alembic_config_from_url(postgres)